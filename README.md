# My Expense

Daily expense managing iOS app project

# Screenshots

<img src="Screenshots/launch.png"  width="100" height="200">
<img src="Screenshots/home1.png"  width="100" height="200">
<img src="Screenshots/home2.png"  width="100" height="200">
<img src="Screenshots/expenses.png"  width="100" height="200">
<img src="Screenshots/categories.png"  width="100" height="200">
<img src="Screenshots/expense-detail.png"  width="100" height="200">
<img src="Screenshots/edit-expense.png"  width="100" height="200">
<img src="Screenshots/edit-category.png"  width="100" height="200">