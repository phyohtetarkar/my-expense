//
//  ExpenseDetail.swift
//  MyExpense
//
//  Created by OP-Macmini3 on 8/15/18.
//  Copyright © 2018 Phyo Htet Arkar. All rights reserved.
//

import Foundation

struct ExpenseDetail {
    var date: Date
    var amount: Double
}
